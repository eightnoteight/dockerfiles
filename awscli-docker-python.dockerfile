FROM python:3.6.3-jessie

# install awscli
RUN pip install awscli --upgrade

# install docker
RUN curl -fsSL get.docker.com | sh
ENTRYPOINT service docker start

